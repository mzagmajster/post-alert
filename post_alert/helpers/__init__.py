from sqlalchemy import or_

from post_alert import app, session
from post_alert.models import Post


class User(object):
	def __init__(self):
		self.is_authenticated = True
		self.is_active = True
		self.is_anonymous = False

	def get_id(self):
		return u'1'

	@property
	def username(self):
		return app.config['ADMIN_USERNAME']

	@property
	def password(self):
		return app.config['ADMIN_PASSWORD']


def get_posts(filters):
	q = session.query(Post)

	if filters['seen'] and not filters['unseen']:
		q = q.filter(Post.seen!=None)
	elif not filters['seen'] and filters['unseen']:
		q = q.filter(Post.seen==None)

	if 'from' in filters:
		q = q.filter(Post.created >= filters['from'])

	if 'to' in filters:
		q = q.filter(Post.created <= filters['to'])

	return q.all()
