from wtforms import Form, StringField, PasswordField, BooleanField, SubmitField, DateTimeField
from wtforms.validators import DataRequired, Optional
from flask_babel import _


class LoginForm(Form):
	username    = StringField(_("Username"), validators=[DataRequired(_("This field is required."))])
	password    = PasswordField(_("Password"), validators=[DataRequired(_("This field is required."))])
	remember_me = BooleanField(_("Remember me"))
	submit      = SubmitField(_("Login"))


class PostFilterForm(Form):
	seen    = BooleanField(_("Seen"))
	unseen  = BooleanField(_("Unseen"))
	d_from  = DateTimeField(_("From"), format='%d. %m. %Y %H:%M:%S', validators=[Optional()])
	d_to    = DateTimeField(_("To"), format='%d. %m. %Y %H:%M:%S', validators=[Optional()])
	submit  = SubmitField(_("Filter posts"))
