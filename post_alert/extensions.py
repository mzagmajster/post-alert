from flask import g
from flask_login import current_user

from post_alert import babel, lm, app
from post_alert.helpers import User


@babel.localeselector
def locale_selector():
	return 'en'


@lm.user_loader
def user_loader(user_id):
	if user_id != u'1':
		return None
	return User()


@app.before_request
def before_request():
	g.user = current_user
	g.fixed_bottom = False
