from post_alert.config.configurator import configure_flask_app, configure_sqlalchemy, configure_extensions


app = configure_flask_app()

engine, Base, session = configure_sqlalchemy(app)

babel, lm, csrf = configure_extensions(app)

from post_alert import models, extensions, views
