from random import randint
from datetime import datetime
from sqlalchemy import Integer, Column, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from post_alert import Base, session


class ModelSkeleton(object):
	def delete(self):
		session.delete(self)
		session.commit()

	def save(self):
		session.add(self)
		session.commit()


class Source(Base, ModelSkeleton):
	__tablename__ = 'source'

	id          = Column(Integer, primary_key=True)
	link        = Column(String(200))
	tag         = Column(String(20), unique=True)
	tag_color   = Column(String(20))

	def __init__(self, link, tag):
		self.link = link
		self.tag = tag
		exists = True
		while exists:
			self.tag_color = '#'
			i = 0
			while i < 3:
				c = hex(randint(0, 255))
				self.tag_color += c[2:]
				i += 1

			if not session.query(Source).filter(Source.tag_color==self.tag_color).count():
				exists = False


class Post(Base, ModelSkeleton):
	__tablename__ = 'post'

	id              = Column(Integer, primary_key=True)
	post_title      = Column(String)
	post_content    = Column(String)
	post_identifier = Column(String)
	created         = Column(DateTime)
	seen            = Column(DateTime)
	source_id       = Column(Integer, ForeignKey('source.id'), nullable=False)

	# Helpers.
	source = relationship('Source', backref='posts')

	def __init__(self, *args, **kwargs):
		allowed_columns = ['post_title', 'post_content', 'post_identifier', 'created', 'seen', 'source_id']
		for k, v in kwargs.iteritems():
			if k not in allowed_columns:
				raise NotImplementedError
			setattr(self, k, v)

		# If 'created' attribute is not set set it implicitly.
		c = None
		try:
			c = kwargs['created']
		except KeyError:
			pass

		if c is None:
			self.created = datetime.utcnow()

	def mark_as(self, s):
		if s == 'seen':
			self.seen = datetime.utcnow()
		else:
			self.seen = None



