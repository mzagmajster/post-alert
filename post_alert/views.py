from flask import render_template, request, redirect, url_for, flash, abort, g
from flask_login import login_required, login_user, logout_user, current_user
from flask_babel import _
from bcrypt import checkpw
from pytz import timezone

from post_alert import app, session
from post_alert.models import Post
from post_alert.forms import LoginForm, PostFilterForm
from post_alert.helpers import User, get_posts
from post_alert.trackers import increment_new_posts, decrement_new_posts


# Auth

@app.route('/', methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('list_posts'))

	form = LoginForm(request.form)
	posts_count = session.query(Post).filter(Post.seen==None).count()

	if request.method == 'POST':
		if form.validate():
			if form.username.data == app.config['ADMIN_USERNAME'] and \
							checkpw(form.password.data.encode('utf8'), app.config['ADMIN_PASSWORD']):
				login_user(User(), form.remember_me.data)
				flash(_("Login succeeded."), 'success')
				return redirect(url_for('list_posts'))
			else:
				flash(_("Username or password is invalid."), 'danger')

	# This view should have fixed bottom to improve design.
	g.fixed_bottom = True
	return render_template('login.html', form=form, posts_count=posts_count)


@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))


# Standard

@app.route('/about.html')
def about():
	return render_template('about.html')


# Posts

@app.route('/list', methods=['GET'])
@login_required
def list_posts():
	form = PostFilterForm(request.args)
	posts = list()

	app.logger.debug('seen {0}'.format(form.seen.data))
	app.logger.debug('unseen {0}'.format(form.unseen.data))
	app.logger.debug('d_form {0}'.format(form.d_from.data))
	app.logger.debug('d_to {0}'.format(form.d_to.data))

	if not form.validate():
		flash(_("Form validation error."), 'danger')
		app.logger.debug(form.errors)
		return render_template('p-list.html', form=form, posts=posts)

	if not form.seen.data and not form.unseen.data:
		form.unseen.data = True

	filters = {
		'seen': form.seen.data,
		'unseen': form.unseen.data,
	}
	t = timezone('Europe/Ljubljana')

	if form.d_from.data is not None:
		filters['from'] = t.localize(form.d_from.data, is_dst=None)

	if form.d_to.data is not None:
		filters['to'] = t.localize(form.d_to.data, is_dst=None)

	posts = get_posts(filters)
	return render_template('p-list.html', form=form, posts=posts)


@app.route('/mark/<path:status>/<int:post_id>')
@login_required
def mark_as(status, post_id):
	allowed_statuses = ['seen', 'unseen']
	if status not in allowed_statuses:
		return abort(404)

	post = session.query(Post).get(post_id)
	if post is None:
		return abort(404)

	post.mark_as(status)
	post.save()

	if status == 'seen':
		decrement_new_posts(1)
	else:
		increment_new_posts(1)

	flash(
		_("Post with ID: {0} and title: '{1}' has been marked as '{2}'.").format(post.id, post.post_title, status),
		'success'
	)

	return redirect(url_for('list_posts'))


@app.route('/delete/<int:post_id>')
@login_required
def delete_post(post_id):
	post = session.query(Post).get(post_id)
	if post is None:
		flash(_("Post with ID: {0} not found.").format(post_id), 'warning')

	# We must keep at least one post in database (newest) for system to work correctly.
	n = session.query(Post).order_by(Post.created.desc()).first()
	pid = post.id
	post_title = post.post_title
	if n.id != post.id:
		post.delete()
		flash(
			_("Post with ID: {0} and title: '{1}' has been successfully removed.").format(pid, post_title),
			'success'
		)
	else:
		flash(
			_("Post with ID: {0} and title: '{1}' is the newest in database and it cannot be deleted.").
			format(pid, post_title),
			'danger'
		)

	return redirect(url_for('list_posts'))


