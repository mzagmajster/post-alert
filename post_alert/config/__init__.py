# str: Database connection string for SQLAlchemy
DATABASE_URI = None

# bool: Application mode.
DEBUG = False

# str: Server name (with port number).
SERVER_NAME = None

# str|None: Application root.
APPLICATION_ROOT = None

# str: Username used in application.
ADMIN_USERNAME = None

# str: Username password.
ADMIN_PASSWORD = None

# bool: CSRF protection
WTF_CSRF_ENABLED = True

# str: Secret key.
SECRET_KEY = None

