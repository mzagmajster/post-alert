from flask import Flask
from flask_babel import Babel, lazy_gettext as _
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from flask_snipsets import CustomJSONEncoder, MomentJS


def configure_flask_app():
	app = Flask('post_alert')
	f = '/'.join([app.instance_path, 'config.py'])
	app.config.from_pyfile(f)
	app.json_encoder = CustomJSONEncoder
	app.jinja_env.globals['MomentJS'] = MomentJS
	app.jinja_env.add_extension('jinja2.ext.i18n')
	return app


def configure_sqlalchemy(app):
	engine = create_engine(app.config['DATABASE_URI'])
	Base = declarative_base()
	Session = sessionmaker(bind=engine)
	session = Session()
	return engine, Base, session


def configure_extensions(app):
	babel = Babel()
	babel.init_app(app)

	lm = LoginManager()
	lm.init_app(app)
	lm.login_view = 'login'
	lm.login_message = _("Please login to access this page.")
	lm.login_message_category = 'info'

	csrf = CSRFProtect()
	csrf.init_app(app)

	return babel, lm, csrf
