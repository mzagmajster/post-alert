from redis import StrictRedis

PS_NEW_POSTS_VAR = 'PS_NEW_POSTS_VAR'

redis = StrictRedis('localhost')


def increment_new_posts(new_posts):
	cnp = int(redis.get(PS_NEW_POSTS_VAR))

	cnp += new_posts
	redis.set(PS_NEW_POSTS_VAR, cnp)
	return cnp


def decrement_new_posts(by):
	cnp = int(redis.get(PS_NEW_POSTS_VAR))

	cnp -= by
	if cnp < 0:
		cnp = 0

	redis.set(PS_NEW_POSTS_VAR, cnp)
	return cnp
