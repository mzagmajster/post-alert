### Installation

* Create file `instance/config.py` and configure app that it matches your needs.
* Copy `alembic.ini.example` to `alembic.ini` and configure it in the way that configuration matches your needs.
* Install package with `python setup.py develop`.
