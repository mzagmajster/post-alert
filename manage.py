import click, bcrypt
from redis import StrictRedis

from post_alert import engine, Base, app
from post_alert.trackers import PS_NEW_POSTS_VAR, redis


@click.group()
def cli():
	pass


@click.group('db')
def db():
	pass


@click.command('create')
def db_create():
	print type(Base.metadata)
	Base.metadata.create_all(bind=engine)


@click.command('init', help="Prepare Redis database.")
def db_init():
	redis.set(PS_NEW_POSTS_VAR, 0)


@click.command('drop')
def db_drop():
	Base.metadata.drop_all(engine)


@click.group('config')
def config():
	pass


@click.command('create-password')
def config_create_password():
	s = str(raw_input('Enter password for admin user: '))
	h = bcrypt.hashpw(s, bcrypt.gensalt())
	print 'Initialize ADMIN_PASSWORD with: "{0}".'.format(h)


@click.command('run-server')
def run_server():
	host = app.config['SERVER_NAME'].split(':')[0]
	app.run(host=host)


db.add_command(db_create)
db.add_command(db_init)
db.add_command(db_drop)

config.add_command(config_create_password)

cli.add_command(db)
cli.add_command(config)

cli.add_command(run_server)


if __name__ == '__main__':
	cli()

