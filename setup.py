from setuptools import setup

setup(
	name='post-alert',
	version='1.0.0',
	packages=['instance', 'post_alert', 'post_alert.config', 'post_alert.trackers'],
	url='https://maticzagmajster.ddns.net/',
	license='',
	author='Matic Zagmajster',
	author_email='zagm101@gmail.com',
	description='Alert user every time new important post is posted on the web.'
)
