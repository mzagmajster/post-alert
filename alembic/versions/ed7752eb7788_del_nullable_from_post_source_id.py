"""del nullable from post source id

Revision ID: ed7752eb7788
Revises: 
Create Date: 2017-08-04 17:41:35.033568

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ed7752eb7788'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
	op.alter_column('post', 'source_id', nullable=True)


def downgrade():
	post_col = sa.table('post', sa.column('source_id'))
	op.execute(post_col.update().values(source_id=-1))
	op.alter_column('post', 'source_id', nullable=False)
	print 'Records with column post.source_id previously set to NULL now have value of -1. Please update records with ' \
			'appropriate key.'
